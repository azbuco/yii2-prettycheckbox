<?php

namespace azbuco\prettycheckbox;

use yii\helpers\Html;

class PrettyCheckbox extends PrettyCore
{

    public function getInput()
    {
        $this->options['label'] = null;
        if ($this->hasModel()) {
            return Html::activeCheckbox($this->model, $this->attribute, $this->options);
        }
        return Html::checkbox($this->name, $this->value, $this->options);
    }
}
