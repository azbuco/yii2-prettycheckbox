<?php

namespace azbuco\prettycheckbox;

use yii\helpers\Html;
use yii\widgets\InputWidget;

class PrettyCore extends InputWidget
{
    /**
     * Shape constants
     */
    const SHAPE_DEFAULT = 'p-default';
    const SHAPE_CURVE = 'p-default p-curve';
    const SHAPE_ROUND = 'p-default p-round';
    const SHAPE_SWITCH = 'p-switch';

    /**
     * Shape switch outline constants
     */
    const APPEARANCE_OUTLINE = 'p-outline';
    const APPEARANCE_FILL = 'p-fill';
    const APPEARANCE_THICK = 'p-thick';

    /**
     * Context constants
     */
    const CONTEXT_PRIMARY = 'p-primary';
    const CONTEXT_SUCCESS = 'p-success';
    const CONTEXT_INFO = 'p-info';
    const CONTEXT_WARNING = 'p-warning';
    const CONTEXT_DANGER = 'p-danger';
    
    /**
     * Animations
     */
    const ANIMATION_SMOOTH = 'p-smooth';
    const ANIMATION_JELLY = 'p-jelly';
    const ANIMATION_TADA = 'p-tada';
    const ANIMATION_ROTATE = 'p-rotate';
    const ANIMATION_PULSE = 'p-pulse';

    public $label;
    public $shape = 'p-default';
    public $switchType;
    public $context;
    public $appearance;
    public $bigger = false;
    public $animation;
    public $icon;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $this->registerBundle();

        $stateOptions = ['class' => 'state'];

        if ($this->context) {
            Html::addCssClass($stateOptions, $this->context);
        }

        $prettyOptions = ['class' => 'pretty'];
        Html::addCssClass($prettyOptions, $this->shape);
        if ($this->appearance) {
            Html::addCssClass($prettyOptions, $this->appearance);
        }
        if ($this->bigger) {
            Html::addCssClass($prettyOptions, 'p-bigger');
        }
        if ($this->animation) {
            Html::addCssClass($prettyOptions, $this->animation);
        }
        if ($this->icon) {
            Html::addCssClass($prettyOptions, 'p-icon');
        }
        
        

        $input = $this->getInput();
        $label = Html::label($this->label);
        $state = Html::tag('div', $this->icon . $label, $stateOptions);
        $widget = Html::tag('div', $input . $state, $prettyOptions);

        echo $widget;
    }

    protected function registerBundle()
    {
        $view = $this->getView();
        PrettyCheckboxAsset::register($view);
    }
}
