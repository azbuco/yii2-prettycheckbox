<?php

namespace azbuco\prettycheckbox;

use yii\web\AssetBundle;

class PrettyCheckboxAsset extends AssetBundle
{
    public $sourcePath = '@azbuco/prettycheckbox/assets';
    public $css = [
        'css/pretty-checkbox.min.css',
    ];
    public $publishOptions = [
        'forceCopy' => YII_DEBUG,
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

}
